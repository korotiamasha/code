"use strict";

(function ($) {
    /*sliders*/
    var cartoonSlider = new Swiper(".cartoon-swiper", {
        slidesPerView: "auto",
        spaceBetween: 15,
        loop: false,
        pagination: {
            el: '.cartoon-swiper-pagination',
            type: 'fraction',
        },
        scrollbar: {
            el: '.cartoon-swiper-scrollbar',
            draggable: true
        },
        breakpoints: {
            575: {
                spaceBetween: 20,
            },
            991: {
                spaceBetween: 38,
            }
        }
    });
    /*modal close*/

    $('.code-accordion__header').on('click', function () {
        $(this).closest('.code-accordion').toggleClass('active');
    })

    /*tabs*/
    $('.code-tabs__item').on('click', function () {
        let tab = $(this).attr('data-tab'),
            tabs = $('.code-tabs');

        if ($(window).width() < 992) {
            tabs.addClass('hide');
            $('#' + tab).addClass('show');
        } else {
            if (!($(this).hasClass('active'))) {
                $('.code-tabs__item.active').removeClass('active');
                $('.code-tab-content').removeClass('active');

                $(this).addClass('active');
                $('#' + tab).addClass('active');
            }
        }
    })

    $('.code-tab-content__header-back').on('click', function () {
        let tabs = $('.code-tabs'),
            item = $(this).closest('.code-tab-content');

        tabs.removeClass('hide');
        item.removeClass('show');
    })

    $('.code-tab-content__header').on('click', function () {
        let tabId = $(this).parent().attr('id');

        if (!($(this).parent().hasClass('active'))) {
            $('.code-tabs__item.active').removeClass('active');
            $('.code-tab-content').removeClass('active');

            $(this).parent().addClass('active');
            $('*[data-tab=' + tabId + ']').addClass('active');
        }
    })

    $('.code-header__burger').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.code-header').removeClass('open');
            $('body').removeClass('modal-open');
        } else {
            $(this).addClass('active');
            $('.code-header').addClass('open');
            $('body').addClass('modal-open');
        }
    })


    /*click on banner*/
    $('.code-main-banner').on('click', function () {
        let h = $('#block2').offset().top;
        console.log(h)
        $('html, body').animate({scrollTop:h},1000);
    })


    $(window).scroll(function(){
        let top = $('#block2').offset().top;
        if($(this).scrollTop()>=top){
            $('.js-header').addClass('fixed');
        } else {
            $('.js-header').removeClass('fixed');
        }
    })

})(jQuery);
